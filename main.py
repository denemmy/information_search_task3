#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
if sys.version_info[0] != 3 or sys.version_info[1] < 4:
    print("This script requires python version 3.4")
    sys.exit(1)

import codecs
from os import mkdir
from os.path import join, exists, isfile
import pymorphy2
import re
from timeit import default_timer as timer
import json
import numpy as np

def vector_norm(vec):
    norm_value = np.linalg.norm(vec)
    if norm_value > 1e-9:
        return vec / norm_value
    else:
        return vec

def split_to_sentences(text):
    splits = re.split('\.|\n', text)
    cleared = []
    for split in splits:
        split = split.strip()
        if len(split) == 0:
            continue
        cleared.append(split)
    return cleared

def analyze_text(morph, text_data):
    stats_dict = dict()
    lexemes = re.findall(r"\b[а-яА-Я]+\b", text_data)
    for idx, lex in enumerate(lexemes):
        word = morph.parse(lex)[0]
        normal_word = word.normal_form
        if normal_word in stats_dict:
            stats_dict[normal_word] += 1
        else:
            stats_dict[normal_word] = 1
    freq = []
    for key in stats_dict:
        freq.append((key, stats_dict[key]))
    freq = sorted(freq, key=lambda p: p[1], reverse=True)
    return freq, stats_dict

def compute_docs_descriptors(stats_dict_docs, all_words, idf_vector):

    descrs = []
    for stats_dict in stats_dict_docs:
        w = []
        for word in all_words:
            tf = 0
            if word in stats_dict:
                tf = stats_dict[word]
            w.append(tf)
        w = np.array(w) * idf_vector
        # w = np.array(w)
        w = vector_norm(w)
        descrs.append(w)

    return descrs


def compute_idf(stats_dict_docs, all_words):
    N = len(stats_dict_docs)
    idf_res = []
    for word in all_words:
        df = 0
        for doc_stats in stats_dict_docs:
            if word in doc_stats:
                df += 1
        if df == 0:
            idf_res.append(0.0)
        else:
            idf_res.append(np.log10(N / df))
    return np.array(idf_res)

def compute_query_descriptor(dict_query, all_words, idf_vector):

    w_counts = []
    for word in all_words:
        if word in dict_query:
            w_counts.append(dict_query[word])
        else:
            w_counts.append(0.0)

    w_counts = np.array(w_counts)

    return vector_norm(idf_vector * w_counts)
    # return vector_norm(w_counts)

def remove_shortened_words(text):
    pattern1 = re.compile(r'\b(\w)\.')
    text = pattern1.sub('\g<1> ', text)

    skip_shorts = ['см', 'прим', 'гг', 'др', 'чел', 'тыс', 'ст', 'км', 'см']
    pattern2_text = ''
    for idx, sh in enumerate(skip_shorts):
        if idx > 0:
            pattern2_text += r'|'
        pattern2_text += r'\b({})\.'.format(sh)
    pattern2_text = r'(?:{})'.format(pattern2_text)
    pattern2 = re.compile(pattern2_text)
    text = pattern2.sub('\g<1> ', text)
    return text

def load_wiki_pages(filenames):

    full_text = ''
    for name in filenames:
        with open(join('wikidata', name), 'r', encoding='utf-8') as f:
            content = f.read()
            for line in content.splitlines():
                line = line.strip()
                if len(line) == 0:
                    continue
                full_text += '{}\n'.format(line)

    full_text = remove_shortened_words(full_text)
    sents = split_to_sentences(full_text)
    return sents

def make_search(morph, fact, pages, fact_name, output_dir):
    top_num = 10

    docs = load_wiki_pages(pages)

    print('{}: number of docs: {}'.format(fact_name, len(docs)))

    # analyzing
    print('{}: analyzing...'.format(fact_name))
    freq_query, dict_query = analyze_text(morph, fact)
    freq_docs_data = [analyze_text(morph, doc) for doc in docs]

    all_words = dict(dict_query)
    freq_docs = []
    stats_dict_docs = []
    for freq_doc, doc_stats_dict in freq_docs_data:
        all_words.update(doc_stats_dict)
        stats_dict_docs.append(doc_stats_dict)
        freq_docs.append(freq_doc)

    print('number of words: {}'.format(len(all_words)))

    idf_vector = compute_idf(stats_dict_docs, all_words)
    query_descriptor = compute_query_descriptor(dict_query, all_words, idf_vector)
    docs_descrs = compute_docs_descriptors(stats_dict_docs, all_words, idf_vector)

    # print('{}: idf vector: {}'.format(fact_name, idf_vector))
    # print('{}: query descriptor: {}'.format(fact_name, query_descriptor))

    # range
    scores = np.array([np.dot(doc_descr, query_descriptor) for doc_descr in docs_descrs])
    ranges = np.argsort(scores)[::-1][:top_num]

    print('{0}: most scored doc with score {1:.4f}:\n{0}: index: {2}'.format(fact_name, scores[ranges[0]], ranges[0]))

    outname = '{}_results.txt'.format(fact_name)
    with open(join(output_dir, outname), 'w', encoding='utf-8') as f:
        f.write('{}\n'.format(fact))
        f.write('top {}:\n'.format(len(ranges)))
        for idx in ranges:
            f.write('========\n')
            f.write('{}:\n{}\n'.format(scores[idx], docs[idx]))


if __name__ == '__main__':

    output_dir = 'output'
    if not exists(output_dir):
        mkdir(output_dir)

    morph = pymorphy2.MorphAnalyzer()

    fact1 = 'Эпоха мира и процветания Англии началась с войны и закончилась войной.'
    pages1 = ['Эдвардианская_эпоха.txt', 'Англо-бурская_война_(1899-1902).txt', 'Первая_мировая_война.txt']
    fact1_name = 'fact1'

    make_search(morph, fact1, pages1, fact1_name, output_dir)

    fact2 = 'Великий русский писатель оставил последние деньги в первом российском кафе'
    pages2 = ['Достоевский_Фёдор_Михайлович.txt', 'Доминик_(кафе).txt']
    fact2_name = 'fact2'

    make_search(morph, fact2, pages2, fact2_name, output_dir)

    fact3 = 'Прибыв на крейсер для встречи гостей, островитяне попали в плен и капитулировали'
    pages3 = ['USS_Charleston_(C-2).txt', 'Гуам.txt', 'Захват_Гуама.txt', 'Капитуляция.txt']
    fact3_name = 'fact3'

    make_search(morph, fact3, pages3, fact3_name, output_dir)

