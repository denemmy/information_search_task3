#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
if sys.version_info[0] != 3 or sys.version_info[1] < 4:
    print("This script requires python version 3.4")
    sys.exit(1)

import wikipedia
from os.path import join, exists, isfile
from os import mkdir
import re

def get_page_data(page_title):

    try:
        wikipedia.set_lang('ru')
        page_data = wikipedia.page(title=page_title)
    except wikipedia.exceptions.WikipediaException as e:
        print('failed to load wiki page')
        print(e)
        return []

    # total_text = ''
    # skip_sections = ['примечания', 'литература']
    # for section in page_data.sections:
    #     if section.lower() in skip_sections:
    #         continue
    #     total_text += page_data.section(section)

    return page_data.content

def load_wiki_pages(pages):

    print('loading wiki pages...')
    docs = []
    for page_title in pages:
        docs.append(get_page_data(page_title))
    print('done.')
    return docs

if __name__ == '__main__':

    output_dir = 'wikidata'
    if not exists(output_dir):
        mkdir(output_dir)

    # pages = ['Эдвардианская эпоха', 'Англо-бурская война (1899-1902)', 'Первая мировая война']
    # pages = ['Достоевский Фёдор Михайлович', 'Доминик (кафе)']
    pages = ['USS Charleston (C-2)', 'Гуам', 'Захват Гуама', 'Капитуляция']
    docs = load_wiki_pages(pages)

    for idx, doc in enumerate(docs):

        req_name = pages[idx]
        req_name = re.sub(' ', '_', req_name)

        name = '{}.txt'.format(req_name)
        outpath = join(output_dir, name)
        with open(outpath, 'w', encoding='utf-8') as f:
            f.write(doc)
